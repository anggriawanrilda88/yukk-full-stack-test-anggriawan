# Fullstack Tech Challenge
An exercise to create Frontend and Backend API Login, Register and Transaction use Laravel Framework.

# Quick Setup
In this Laravel app, I created two interface platform to provide Frontend and Backend, this improvisation to show my ability in Frontend and Backend(Rest API).
**Prepare Application**
- `PHP 8.3`: Ensure your php version is PHP 8.3.
- `Postgresql`: Database used for this test is Postgresql.
- `file .env`: Create and config your .env with your own configuration, you can see example/copy on file .env.example, database host, port etc.
- `.env API_HOST`: Like I mention above, because ini this app I use two interface to run, ensure you set your "API_HOST" on file .env on right host. So you have to run two localhost port for Frontend and Backend, I will guide bellow.
- `Migrate Database Schema`: Run migrate database to create table. 

**Here Step By Step To Run It**
```bash
# Install app dependency 
composer install

# Run migration script
php artisan migrate

# Run Frontend host with port
php artisan serve --port=3000

# Run Backend host with port(set 'API_HOST' variable, default is localhost:3001)
php artisan serve --port=3001

# Open your browser localhost:3000 and test the web
```

**Feature Used**
- `Authorization and Authentication`: This web has Authentication with JWT web token and simple role management on middleware.
- `Rest API`: Data served use API call so Frontend just call the API to get and process data.
- `Bootstrap 5`: Use Bootsrap 5 UI to make clean website.