<?php

return [
    'JWT_SECRET' => env('JWT_SECRET', '123456789'),
    'API_HOST' => env('API_HOST', 'localhost:3001'),
];
