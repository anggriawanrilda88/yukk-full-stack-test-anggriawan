<?php

use App\Http\Controllers\Web\TransactionController;
use App\Http\Controllers\Web\UserController;
use App\Http\Middleware\LoginSession;
use Illuminate\Support\Facades\Route;

Route::middleware([LoginSession::class])->group(function () {

    // user web with UserController
    Route::get('/', [UserController::class, 'loginView'])->name('loginView');
    Route::post('/login', [UserController::class, 'login'])->name('login');
    Route::get('/logout', [UserController::class, 'logout'])->name('logout');
    Route::get('/register', [UserController::class, 'registerView'])->name('registerView');
    Route::post('/register', [UserController::class, 'register'])->name('register');

    // user web with UserController
    Route::get('/transaction', [TransactionController::class, 'transactionList'])->name('transactionList');
    Route::get('/transaction/create', [TransactionController::class, 'transactionCreateView'])->name('transactionCreateView');
    Route::post('/transaction/create', [TransactionController::class, 'transactionCreate'])->name('transactionCreate');
    Route::get('/transaction/image/{id}', [TransactionController::class, 'transactionImage'])->name('transactionImage');
});
