<?php

use App\Http\Controllers\Api\V1\TransactionController;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Middleware\AuthorizationJwt;
use Illuminate\Support\Facades\Route;

Route::middleware([AuthorizationJwt::class])->group(function () {
    Route::group(['prefix' => 'v1'], function () {
        // users route
        Route::group(['prefix' => 'users'], function () {
            Route::post('/login', [UserController::class, 'login'])->name('users.login');
            Route::post('/register', [UserController::class, 'register'])->name('users.register');
            Route::get('/detail', [UserController::class, 'detail'])->name('users.detail');
        });

        // transaction route
        Route::group(['prefix' => 'transactions'], function () {
            Route::post('/', [TransactionController::class, 'create'])->name('transactions.create');
            Route::get('/history', [TransactionController::class, 'history'])->name('transactions.history');
            Route::get('/topup-image/{id}', [TransactionController::class, 'topupImage'])->name('transactions.topupImage');
        });
    });
});
