<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class User extends Service
{
    const PATH_USER_LOGIN = "/api/v1/users/login";
    const PATH_USER_REGISTER = "/api/v1/users/register";
    const PATH_USER_DETAIL = "/api/v1/users/detail";

    public function loginUser(Request $request)
    {
        $url =  $this->host . self::PATH_USER_LOGIN;
        $response = Http::post($url, [
            'email' => $request->email,
            'password' => $request->password
        ]);

        try {
            if ($response->status() != 200) {
                $errorData = json_decode($response->getBody()->getContents());
                $errors = $errorData->errors;
                if (is_object($errorData->errors)) {
                    $errors = "<ul>";
                    foreach ($errorData->errors as $value) {
                        foreach ($value as $v) {
                            $errors .= "<li>$v</li>";
                        }
                    }
                    $errors .= "</ul>";
                }

                return [
                    'data' => null,
                    'error' => $errors,
                ];
            }
        } catch (\Throwable $th) {
            return [
                'data' => null,
                'error' => "There is trouble happen on the system",
            ];
        }

        return [
            'data' => json_decode($response->getBody()->getContents()),
            'error' => null,
        ];
    }

    public function registerUser(Request $request)
    {
        $url =  $this->host . self::PATH_USER_REGISTER;
        $response = Http::post($url, [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);

        try {
            if ($response->status() != 201) {
                $errorData = json_decode($response->getBody()->getContents());
                $errors = $errorData->errors;
                if (is_object($errorData->errors)) {
                    $errors = "<ul>";
                    foreach ($errorData->errors as $value) {
                        foreach ($value as $v) {
                            $errors .= "<li>$v</li>";
                        }
                    }
                    $errors .= "</ul>";
                }

                return [
                    'data' => null,
                    'error' => $errors,
                ];
            }
        } catch (\Throwable $th) {
            return [
                'data' => null,
                'error' => "There is trouble happen on the system",
            ];
        }

        return [
            'data' => json_decode($response->getBody()->getContents()),
            'error' => null,
        ];
    }

    public function detailUser(Request $request)
    {
        $url =  $this->host . self::PATH_USER_DETAIL;
        $response = Http::withToken($request->session()->get('Authorization'))->get($url);

        try {
            if ($response->status() != 200) {
                $errorData = json_decode($response->getBody()->getContents());
                $errors = $errorData->errors;
                if (is_object($errorData->errors)) {
                    $errors = "<ul>";
                    foreach ($errorData->errors as $value) {
                        foreach ($value as $v) {
                            $errors .= "<li>$v</li>";
                        }
                    }
                    $errors .= "</ul>";
                }

                return [
                    'data' => null,
                    'error' => $errors,
                ];
            }
        } catch (\Throwable $th) {
            return [
                'data' => null,
                'error' => "There is trouble happen on the system",
            ];
        }

        return [
            'data' => json_decode($response->getBody()->getContents()),
            'error' => null,
        ];
    }
}
