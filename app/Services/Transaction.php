<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class Transaction extends Service
{
    const PATH_TRANSACTION_LIST = "/api/v1/transactions/history";
    const PATH_TRANSACTION_CREATE = "/api/v1/transactions";
    const PATH_TRANSACTION_IMAGE = "/api/v1/transactions/topup-image/";

    public function listTransaction(Request $request)
    {

        $paramsUrl = str_replace("/transaction", "",  request()->getRequestUri());
        $url =  $this->host . self::PATH_TRANSACTION_LIST  . $paramsUrl;

        $response = Http::withToken($request->session()->get('Authorization'))->get($url);
        try {
            if ($response->status() != 200) {
                $errorData = json_decode($response->getBody()->getContents());
                $errors = $errorData->errors;
                if (is_object($errorData->errors)) {
                    $errors = "<ul>";
                    foreach ($errorData->errors as $value) {
                        foreach ($value as $v) {
                            $errors .= "<li>$v</li>";
                        }
                    }
                    $errors .= "</ul>";
                }

                return [
                    'data' => null,
                    'error' => $errors,
                ];
            }
        } catch (\Throwable $th) {
            return [
                'data' => null,
                'error' => "There is trouble happen on the system",
            ];
        }

        return [
            'data' => json_decode($response->getBody()->getContents()),
            'error' => null,
        ];
    }

    public function fileTopupTransaction($id)
    {
        $url =  $this->host . self::PATH_TRANSACTION_IMAGE . $id;
        $response = Http::withToken(request()->session()->get('Authorization'))->get($url);

        if ($response->status() != 200) {
            return null;
        }

        return $response;
    }

    public function createTransaction(Request $request)
    {
        $url =  $this->host . self::PATH_TRANSACTION_CREATE;

        // check file is present and has no problem uploading it
        if ($request->file('file')) {
            // get Illuminate\Http\UploadedFile instance
            $image = $request->file('file');

            // post request with attachment
            $response = Http::withToken($request->session()->get('Authorization'))->attach('file', file_get_contents($image), 'photo.jpg', ['Content-Type' => $image->getClientMimeType()])
                ->post($url, $request->all());
        } else {
            $response = Http::withToken($request->session()->get('Authorization'))->post($url, $request->all());
        }

        try {
            if ($response->status() != 201) {
                $errorData = json_decode($response->getBody()->getContents());
                $errors = $errorData->errors;
                if (is_object($errorData->errors)) {
                    $errors = "<ul>";
                    foreach ($errorData->errors as $value) {
                        foreach ($value as $v) {
                            $errors .= "<li>$v</li>";
                        }
                    }
                    $errors .= "</ul>";
                }

                return [
                    'data' => null,
                    'error' => $errors,
                ];
            }
        } catch (\Throwable $th) {
            return [
                'data' => null,
                'error' => "There is trouble happen on the system",
            ];
        }

        return [
            'data' => json_decode($response->getBody()->getContents()),
            'error' => null,
        ];
    }
}
