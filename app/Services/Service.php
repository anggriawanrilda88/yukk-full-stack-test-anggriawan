<?php

namespace App\Services;

use Illuminate\Support\Facades\Config;

class Service
{
    protected $host;

    public function __construct()
    {
        $this->host = Config::get('constants.API_HOST');
    }
}
