<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Transaction;
use App\Services\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{
    public function transactionList(Request $request)
    {
        // get user api service
        $userService = new User;
        $responseUser = $userService->detailUser($request);

        if ($responseUser['error']) {
            return redirect()->route('transactionList', $request->all())
                ->with('errorMessage', $responseUser['error']);
        }

        // create transaction api service
        $transactionService = new Transaction;
        $responseTransaction = $transactionService->listTransaction($request);

        if ($responseTransaction['error']) {
            return redirect()->route('transactionList', $request->all())
                ->with('errorMessage', $responseTransaction['error']);
        }

        // set currency format
        Blade::directive('currency', function ($expression) {
            return "Rp. <?php echo number_format($expression,0,',','.'); ?>";
        });

        // pagination
        $meta = $responseTransaction["data"]->meta;
        $totalData = $meta->count; // Jumlah total data
        $perPage = $meta->perPage; // Jumlah data per halaman
        $totalPages = ceil($totalData / $perPage);

        return view('transaction.histories', [
            'userData' => $responseUser["data"]->data,
            'transactionData' => $responseTransaction["data"]->data,
            'totalPages' => $totalPages,
            'transactionService' => $transactionService,
        ]);
    }

    public function transactionCreateView(Request $request)
    {
        return view('transaction.create');
    }

    public function transactionCreate(Request $request)
    {
        // create transaction api service
        $transactionService = new Transaction;
        $response = $transactionService->createTransaction($request);

        if ($response['error']) {
            return redirect()->route('transactionList')
                ->with('errorMessage', $response['error']);
        }

        return redirect()->route('transactionList')
            ->with('successMessage', "Transaction successfully created.");
    }

    public function transactionImage(Request $request)
    {
        // create transaction api service
        $transactionService = new Transaction;
        $responseTransaction = $transactionService->fileTopupTransaction($request->id);
        return $responseTransaction;
    }
}
