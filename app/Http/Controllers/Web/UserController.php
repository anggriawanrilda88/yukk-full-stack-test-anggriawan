<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function loginView(Request $request)
    {
        return view('user.login');
    }

    public function login(Request $request)
    {
        // get user api service
        $userService = new User;
        $response = $userService->loginUser($request);

        if ($response['error']) {
            return redirect()->route('loginView')
                ->with('errorMessage', $response['error']);
        }

        $request->session()->put('Authorization', $response['data']->data->token);
        return redirect()->route('transactionList');
    }

    public function registerView(Request $request)
    {
        return view('user.register');
    }

    public function register(Request $request)
    {
        // get user api service
        $userService = new User;
        $response = $userService->registerUser($request);

        if ($response['error']) {
            return redirect()->route('registerView')
                ->with('errorMessage', $response['error']);
        }

        return redirect()->route('registerView')
            ->with('successMessage', "User successfully registered, please Sign In and use the feature.");
    }

    public function logout(Request $request)
    {
        $request->session()->forget('Authorization');
        return redirect()->route('loginView');
    }
}
