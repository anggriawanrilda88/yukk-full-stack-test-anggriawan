<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomResponse;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => [
                'required',
            ],
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Login failed",
                "errors" => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        // get user from email
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json([
                "message" => "Login failed",
                "errors" => "User not found"
            ], Response::HTTP_NOT_FOUND);
        }

        // validate password
        $checkPassword = password_verify($request->password, $user->password);
        if (!$checkPassword) {
            return response()->json([
                "message" => "Login failed",
                "errors" => "Invalid password"
            ], Response::HTTP_BAD_REQUEST);
        }

        // set payload for generate token
        $payload = array(
            "userId" => $user->id,
            "name" => $user->name,
            "email" => $user->email,
            "exp" => strtotime("+4 week")
        );

        // set token
        $jwt = JWT::encode($payload, Config::get('constants.JWT_SECRET'), 'HS256');

        return new CustomResponse('Login successfully', ["token" => $jwt]);
    }

    public function detail(Request $request)
    {
        // get user from email
        $user = User::where('id', $request->userId)->first();
        if (!$user) {
            return response()->json([
                "message" => "Login failed",
                "errors" => "User not found"
            ], Response::HTTP_NOT_FOUND);
        }

        return new CustomResponse('Get user data successfully', $user);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required',
            'password' => [
                'required',
                'string',
                'min:8',
                'regex:/[A-Z]/',            // one uppercase
                'regex:/[!@#$%^&*()\-_=+{};:,<.>]/'  // one symbol
            ],
        ], [
            'regex' => 'Password must have one uppercase, and one symbol'
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Registration failed",
                "errors" => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        // encript password
        $passwordHash = password_hash($request->password, PASSWORD_BCRYPT, ['cost' => 9]);

        try {
            $user = User::create([
                'name'     => $request->name,
                'email'     => $request->email,
                'password'   => $passwordHash
            ]);
        } catch (\PDOException $e) {
            if ($e->errorInfo[0] == '23505') {
                return response()->json([
                    "message" => "Registration failed",
                    "errors" => "Email already taken"
                ], 400);
            }
            return response()->json([
                "message" => "Registration failed",
                "errors" => $e->errorInfo[2],
            ], 400);
        } catch (\Throwable $e) {
            return response()->json([
                "message" => "Registration failed",
                "errors" => $e->getMessage()
            ], 400);
        }

        return new CustomResponse('User successfully registered!', $user);
    }
}
