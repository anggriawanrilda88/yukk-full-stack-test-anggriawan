<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CustomResponse;
use App\Models\Transaction;
use App\Models\User;
use Hamcrest\Type\IsString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class TransactionController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required', Rule::in(['topup', 'transaction'])],
            'amount' => 'required|integer|min:10000|max:5000000',
            'description' => 'required',
            'file' => 'required_if:type,topup|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Transaction/Top Up failed",
                "errors" => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        DB::beginTransaction();
        try {
            $file = $request->file('file');
            $fileName = null;
            if ($request->type == "topup") {
                $fileName = $file->hashName();
                $file->storeAs('public/transaction', $file->hashName());
            }

            $microtime = microtime(true);
            $microseconds = substr(str_replace('.', '', $microtime), 0, 1);
            $transactionCode = 'TRX_' . date('YmdHis') . $microseconds;

            // lock transaction for wallet prevent racing condition
            $user = User::where("id", $request->userId)->lockForUpdate()->first();
            if (!$user) {
                return response()->json([
                    "message" => "Transaction/Top Up failed",
                    "errors" => $request->userId
                ], Response::HTTP_NOT_FOUND);
            }

            // save last wallet
            $lastWallet = $user->wallet;

            // validate wallet enough for transaction
            if ($request->type == "transaction" && $user->wallet < $request->amount) {
                return response()->json([
                    "message" => "Transaction/Top Up failed",
                    "errors" => "Wallet not sufficient"
                ], Response::HTTP_BAD_REQUEST);
            }

            // update wallet depend type transaction
            $user->wallet = $request->type === "topup" ? ($user->wallet + $request->amount) : ($user->wallet - $request->amount);
            $user->save();

            $transaction = Transaction::create([
                'user_id'     => $request->userId,
                'type'     => $request->type,
                'code'     => $transactionCode,
                'amount'     => $request->amount,
                'last_wallet'     => $lastWallet,
                'description'     => $request->description,
                'file'     => $fileName,
            ]);

            DB::commit();
        } catch (\PDOException $e) {
            return response()->json([
                "message" => "Transaction/Top Up failed",
                "errors" => $e->errorInfo[2]
            ], 400);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                "message" => "Transaction/Top Up failed",
                "errors" => $e->getMessage()
            ], 400);
        }

        return new CustomResponse('Transaction/Top Up successfully uploaded', $transaction);
    }

    public function history(Request $request)
    {
        try {
            $page = ($request->page && is_numeric($request->page)) ? $request->page : 1;
            $perPage = ($request->perPage && is_numeric($request->perPage)) ? $request->perPage : 5;
            $limit = $perPage;

            $offset = $page == 1 ? 0 : ($page - 1) * $limit;
            $sort = $request->sort ? $request->sort : 'desc';
            $sortBy = $request->sortBy ? $request->sortBy : 'id';

            $transaction = Transaction::where('user_id', $request->userId);


            // filter code
            if ($request->code) {
                $transaction = $transaction->where('code', $request->code);
            }

            // filter range amount
            $minAmount = ($request->minAmount && is_numeric($request->minAmount)) ? $request->minAmount : null;
            $maxAmount = ($request->maxAmount && is_numeric($request->maxAmount)) ? $request->maxAmount : null;

            if (!is_null($minAmount) && !is_null($maxAmount)) {
                $transaction = $transaction->whereBetween('amount', [$minAmount, $maxAmount]);
            } elseif (!is_null($minAmount)) {
                $transaction = $transaction->where('amount', '>=', $minAmount);
            } elseif (!is_null($maxAmount)) {
                $transaction = $transaction->where('amount', '<=', $maxAmount);
            }

            // filter range date
            if (!is_null($request->startDate) && !is_null($request->endDate)) {
                $transaction = $transaction->whereRaw("to_char(created_at, 'YYYY-MM-DD') between ? and ?", [date("Y-m-d", strtotime($request->startDate)), date("Y-m-d", strtotime($request->endDate))]);
                // dd($transaction->toSql());
            } elseif (!is_null($request->startDate)) {
                $transaction = $transaction->whereRaw("to_char(created_at, 'YYYY-MM-DD') >= ?", [date("Y-m-d", strtotime($request->startDate))]);
            } elseif (!is_null($request->endDate)) {
                $transaction = $transaction->whereRaw("to_char(created_at, 'YYYY-MM-DD') <= ?", [date("Y-m-d", strtotime($request->endDate))]);
            }

            // filter type
            if ($request->type) {
                $transaction = $transaction->where('type', $request->type);
            }

            // filter search by keyword
            if ($request->search) {
                $search = $request->search;
                $transaction = $transaction->where(function ($query) use ($search) {
                    $query->where('code', 'ilike', '%' . $search . '%')
                        ->orWhere('description', 'ilike', '%' . $search . '%');
                });
            }

            // get total list transaction
            $count = $transaction->count();

            // set limit offset
            $transaction = $transaction->limit($limit);
            $transaction = $transaction->offset($offset);

            // set order 
            if ($sort == 'desc') {
                $transaction = $transaction->orderByDesc($sortBy);
            } else {
                $transaction = $transaction->orderBy($sortBy);
            }

            return new CustomResponse('Transaction/Top Up successfully uploaded', $transaction->get(), [
                'count' => $count,
                'page' => (int) $page,
                'perPage' => (int) $perPage,
            ]);
        } catch (\PDOException $e) {
            return response()->json([
                "message" => "Transaction/Top Up failed",
                "errors" => $e->errorInfo[2]
            ], 400);
        } catch (\Throwable $e) {
            return response()->json([
                "message" => "Get list transaction failed",
                "errors" => $e->getMessage()
            ], 400);
        }
    }

    public function topupImage(Request $request)
    {
        $transaction = Transaction::where('user_id', $request->userId)->where('id', $request->id)->first();
        // if transaction not found
        if (!$transaction) {
            return response()->json(['message' => 'Transaction not found', "errors" => 'Transaction not found'], 404);
        }

        // image topup not found
        if (!$transaction->file) {
            return response()->json(['message' => 'Topup image not found', "errors" => 'Topup image not found'], 404);
        }

        $filePath = $transaction->file;

        // get image from storage
        $fileContents = Storage::disk('public')->get("transaction/" . $filePath);

        // set extension for image
        $contentType = Storage::mimeType($fileContents);

        return response($fileContents)->header('Content-Type', $contentType);
    }
}
