<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class AuthorizationJwt
{
    private $allowedRouteAlias = [
        'users.login' => true,
        'users.register' => true,
        'users.detail' => false,
        'transactions.create' => false,
        'transactions.history' => false,
        'transactions.topupImage' => false,
    ];

    public function handle(Request $request, Closure $next): Response
    {
        try {
            // get route alias
            $routeAlias = $request->route()->action['as'];

            // check route alias to allowed from JWT authorization
            try {
                if ($this->allowedRouteAlias[$routeAlias]) {
                    return $next($request);
                }
            } catch (\Throwable $th) {
                throw new Exception('Route not set on authorization', 403);
            }

            // get headers
            $headers = $request->header();

            // check authorization token
            if (!isset($headers['authorization'])) {
                throw new Exception('Unauthorized', 401);
            }

            // split to remove bearer on token
            $authData = explode(' ', $headers['authorization'][0]);
            $token = $authData[1];

            // extract enkripsi data ke array
            $decoded = JWT::decode($token, new Key(Config::get('constants.JWT_SECRET'), 'HS256'));
            if ($decoded) {
                $request['userId'] = $decoded->userId;
                $request['name'] = $decoded->name;
                $request['email'] = $decoded->email;
            }

            return $next($request);
        } catch (\Throwable $e) {
            return response()->json(["message" => "Login failed", "errors" => $e->getMessage()], $e->getCode());
        }
    }
}
