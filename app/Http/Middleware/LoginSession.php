<?php

namespace App\Http\Middleware;

use App\Services\User;
use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response;

class LoginSession
{
    private $allowedRouteAlias = [
        'loginView' => false,
        'login' => false,
        'logout' => true,
        'registerView' => false,
        'register' => false,
        'transactionList' => true,
        'transactionCreateView' => true,
        'transactionCreate' => true,
        'transactionImage' => true,
    ];

    public function handle(Request $request, Closure $next): Response
    {
        // get route alias
        $routeAlias = $request->route()->action['as'];

        // check route alias to allowed from JWT authorization
        try {
            if (!$this->allowedRouteAlias[$routeAlias]) {
                if ($request->session()->get('Authorization')) {
                    return redirect()->route('transactionList');
                }
                return $next($request);
            }

            if (!$request->session()->get('Authorization')) {
                return redirect()->route('loginView');
            }

            // get user api service
            $userService = new User;
            $responseUser = $userService->detailUser($request);

            if (!$responseUser['data']) {
                $request->session()->forget('Authorization');
                return redirect()->route('logout');
            }
        } catch (\Throwable $th) {
            return redirect()->route('loginView');
        }

        return $next($request);
    }
}
