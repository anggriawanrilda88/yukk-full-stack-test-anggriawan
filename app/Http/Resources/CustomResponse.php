<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomResponse extends JsonResource
{
    public $message;
    public $meta;

    /**
     * __construct
     *
     * @param  mixed  $status
     * @param  mixed  $message
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($message, $resource, $meta = null)
    {
        parent::__construct($resource);
        $this->message = $message;
        $this->meta = $meta;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->meta) {
            return [
                'message' => $this->message,
                'data' => $this->resource,
                'meta' => [
                    "count" => $this->meta["count"],
                    "page" => $this->meta["page"],
                    "perPage" => $this->meta["perPage"],
                ],
            ];
        }

        return [
            'message' => $this->message,
            'data' => $this->resource,
        ];
    }
}
