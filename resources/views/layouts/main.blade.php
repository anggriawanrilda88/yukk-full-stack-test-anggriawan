<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <style>
        html,
        body {
            height: 100%;
        }

        body {
            display: flex;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-default {
            width: 100%;
            max-width: 350px;
            padding: 15px;
            margin: auto;
        }

        .form-default .checkbox {
            font-weight: 400;
        }

        .form-default .form-floating:focus-within {
            z-index: 2;
        }

        .form-default input.login-email {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .form-default input.login-password {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .form-default button[type="submit"] {
            margin-top: 10px;
        }

        .invalid-feedback {
            margin-bottom: 10px;
        }
    </style>

    {{-- push css on page --}}
    @stack('css')
</head>

<body>
    <div class="container">
        @yield('content')
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    {{-- custom JS --}}
    @stack('scripts')
</body>

</html>
