@extends('layouts.main')

@section('title', 'Register')

@push('css')
    <style>
        .message-error-register ul,
        .message-success-register ul {
            padding: 0 18px;
            margin: 0;
        }

        .login {
            text-align: right;
            margin-top: 10px;
            font-size: 14px;
        }
    </style>
@endpush

@section('content')
    <main class="form-default">
        <form method="POST" action="{{ route('register') }}" id="registerForm" class="needs-validation" novalidate>
            @csrf

            <img class="mb-4" src="https://yukk.co.id/images/YUKK.png" alt="" height="37">
            <h1 class="h3 mb-3 fw-normal">Register Form</h1>


            @if (session('errorMessage'))
                <div class="alert alert-danger" style="font-size: 14px;" role="alert">
                    <div class="message-error-register">{!! session('errorMessage') !!}</div>
                </div>
            @endif

            @if (session('successMessage'))
                <div class="alert alert-success" style="font-size: 14px;" role="alert">
                    <div class="message-success-register">{!! session('successMessage') !!}</div>
                </div>
            @endif

            <div class="form-floating">
                <input type="name" name="name" class="form-control" id="floatingName" placeholder="John Doe" required>
                <div class="invalid-feedback">Fullname is required.</div>
                <label for="floatingName">Fullname</label>
            </div>
            <div class="form-floating">
                <input type="email" name="email" class="form-control" id="floatingEmail" placeholder="name@example.com" required email>
                <div class="invalid-feedback">Email is required and must have right format.</div>
                <label for="floatingEmail">Email address</label>
            </div>
            <div class="form-floating">
                <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password" required>
                <div class="invalid-feedback">Password must have one uppercase, and one symbol</div>
                <label for="floatingPassword">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Sign Up</button>
            <div class="login">Back to Sign In <a href="{{ route('loginView') }}">here</a></div>
        </form>
    </main>
@endsection

@push('scripts')
    <script>
        // // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })()
    </script>
@endpush
