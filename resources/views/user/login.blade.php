@extends('layouts.main')

@section('title', 'Login')

@push('css')
    <style>
        .message-error-login ul {
            padding: 0 18px;
            margin: 0;
        }

        .register {
            text-align: right;
            margin-top: 10px;
            font-size: 14px;
        }
    </style>
@endpush

@section('content')
    <main class="form-default">
        <form method="POST" action="{{ route('login') }}" id="loginForm" class="needs-validation" novalidate>
            @csrf

            <img class="mb-4" src="https://yukk.co.id/images/YUKK.png" alt="" height="37">
            <h1 class="h3 mb-3 fw-normal">Please sign in</h1>


            @if (session('errorMessage'))
                <div class="alert alert-danger" style="font-size: 14px;" role="alert">
                    <div class="message-error-login">{!! session('errorMessage') !!}</div>
                </div>
            @endif

            <div class="form-floating">
                <input type="email" name="email" class="form-control login-email" id="floatingEmail"
                    placeholder="name@example.com" required email>
                <div class="invalid-feedback">Email is required and must have right format.</div>
                <label for="floatingEmail">Email address</label>
            </div>
            <div class="form-floating">
                <input type="password" name="password" class="form-control login-password" id="floatingPassword"
                    placeholder="Password" required>
                <div class="invalid-feedback">Password is required.</div>
                <label for="floatingPassword">Password</label>
            </div>

            <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
            <div class="register">Don't have account, Register <a href="{{ route('register') }}">here</a></div>
            <p class="mt-5 mb-3 text-muted">&copy; 2024 Anggriawan</p>
        </form>
    </main>
@endsection

@push('scripts')
    <script>
        // // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })()
    </script>
@endpush
