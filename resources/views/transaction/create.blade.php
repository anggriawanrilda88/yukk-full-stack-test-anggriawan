@extends('layouts.main')

@section('title', 'Add Transaction')

@push('css')
    <style>
        body {
            display: block;
            align-items: center;
            padding-top: 0;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .create-transaction {
            padding: 10px 0;
        }

        .create-transaction .add-right {
            float: left;
            display: inline-block;
            margin-top: -3px;
        }

        .create-transaction .add-right a {
            color: #f5f5f5;
            background-color: #2a5099;
            padding: 4px 6px;
            border-radius: 3px;
            text-align: center;
            display: inline-block;
            font-size: 14px;
            margin-bottom: 5px;
            text-decoration: none;
        }
    </style>
@endpush

@section('content')
    <main class="form-default create-transaction">
        <br>
        <img class="mb-4" src="https://yukk.co.id/images/YUKK.png" alt="" height="37">
        <form method="POST" action="{{ route('transactionCreate') }}" id="registerForm" class="needs-validation"
            enctype="multipart/form-data" novalidate>
            @csrf

            <h1 class="h3 mb-3 fw-normal" style="text-align: right">
                <div class="add-right"><a href="{{ route('transactionList') }}">Back</a></div> Add Transaction
            </h1>

            @if (session('errorMessage'))
                <div class="alert alert-danger" style="font-size: 14px;" role="alert">
                    <div class="message-error-register">{!! session('errorMessage') !!}</div>
                </div>
            @endif

            @if (session('successMessage'))
                <div class="alert alert-success" style="font-size: 14px;" role="alert">
                    <div class="message-success-register">{!! session('successMessage') !!}</div>
                </div>
            @endif

            <div class="form-floating">
                <select class="form-select" id="floatingSelect" aria-label="Floating label select example" name="type"
                    required>
                    <option selected disabled value="">Choose Transaction Type</option>
                    <option value="topup">Topup</option>
                    <option value="transaction">Transaction</option>
                </select>
                <div class="invalid-feedback">Transaction Type is required.</div>
                <label for="floatingSelect">Transaction Type</label>
            </div>
            <div class="form-floating">
                <input type="file" name="file" class="form-control" id="floatingFile" placeholder="File" required>
                <div class="invalid-feedback">File is required.</div>
            </div>
            <div class="form-floating">
                <input type="text" name="amount" class="form-control" id="floatingAmount" placeholder="name@example.com"
                    required>
                <div class="invalid-feedback">Amount is required.</div>
                <label for="floatingAmount">Amount</label>
            </div>
            <div class="form-floating">
                <input type="text" name="description" class="form-control" id="floatingDescription"
                    placeholder="Description" required>
                <div class="invalid-feedback">Description is required.</div>
                <label for="floatingDescription">Description</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit">Create</button>
        </form>
    </main>
@endsection

@push('scripts')
    <script>
        // // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })();

        $('#floatingSelect').change(function() {
            // Mendapatkan nilai yang dipilih dari dropdown
            var selectedValue = $(this).val();

            // Menonaktifkan input file jika topup dipilih, dan mengaktifkannya jika transaction dipilih
            if (selectedValue === 'transaction') {
                $('#floatingFile').prop('disabled', true);
            } else {
                $('#floatingFile').prop('disabled', false);
            }
        });
    </script>
@endpush
