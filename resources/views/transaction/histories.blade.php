@extends('layouts.main')

@section('title', 'Transaction')

@push('css')
    <style>
        body {
            display: block;
            align-items: center;
            padding-top: 0;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .histories {
            padding: 10px 0;
        }

        .balance {
            font-size: 14px;
            font-weight: bold;
        }

        .histories .balance-left {}

        .histories .balance-right {
            text-align: right
        }

        .histories .balance-right span {
            color: #f5f5f5;
            background-color: rgb(185 59 139);
            padding: 0px 1px 0px 3px;
            border-radius: 3px;
            text-align: center;
            display: inline-block;
        }

        .histories .add-right {
            text-align: right
        }

        .histories .add-right a {
            color: #f5f5f5;
            background-color: #2a5099;
            padding: 4px 6px;
            border-radius: 3px;
            text-align: center;
            display: inline-block;
            font-size: 14px;
            margin-bottom: 5px;
            text-decoration: none;
        }

        .filter {
            font-size: 14px;
            font-weight: bold;
            margin-top: 15px;
        }

        .histories .filter-left {}

        .histories .filter-left input {
            font-size: 14px;
        }

        .histories .filter-left button[type="submit"] {
            margin: 0 0 0 0px;
            border-radius: 0 5px 5px 0;
        }

        .histories .filter-left button[type="submit"] i {
            color: #4f4f4f;
        }

        .histories .filter-left button[type="submit"] i:hover {
            color: black;
        }

        .histories .filter-right {
            text-align: right;
        }

        .histories .filter-right button[type="submit"] {
            margin: 0 0 0 0;
            padding-right: 10px;
            padding-left: 10px;
        }

        .histories .filter-right button[type="submit"] i {
            color: #4f4f4f;
        }

        .histories .filter-right button[type="submit"] i:hover {
            color: black;
        }

        .list-transaction {
            font-size: 14px;
            margin-top: 15px;
        }

        .list-transaction .list {
            padding: 10px;
            border: 1px solid #dee2e6;
            border-radius: 5px;
            background: #ffffff;
        }

        .list-transaction .title {
            margin-bottom: 10px;
        }

        .list-transaction .right {
            text-align: right;
            color: red;
        }

        .list-transaction .right.topup {
            text-align: right;
            color: rgb(0, 222, 0);
        }

        .list-transaction .right.upload a {
            text-align: right;
            color: black;
            text-decoration: none;
            font-size: 15px;
        }

        .list-transaction .right.upload a:hover {
            color: black;
            font-weight: bold;
        }

        .list-transaction nav {
            margin-top: 10px;
        }

        .list-transaction nav a {
            font-size: 14px;
            color: #4f4f4f;
        }

        .add-transaction {
            margin-top: 15px;
            margin-bottom: -10px;
        }

        .transaction-date {
            font-size: 13px;
        }

        .transaction-code {
            font-size: 10px;
        }

        .transaction-topup-modal,
        .transaction-description-modal {
            cursor: pointer;
        }

        .active>.page-link,
        .page-link.active {
            background-color: #2a5099;
        }

        .logout {
            text-decoration: none;
            background: red;
            color: white;
            border-radius: 5px;
            padding: 5px;
        }

        .fullname {
            font-size: 13px;
        }
    </style>
@endpush

@section('content')
    <main class="form-default histories">
        <br>
        <div class="row">
            <div class="col">
                <img class="mb-4" src="https://yukk.co.id/images/YUKK.png" alt="" height="37">
            </div>
            <div class="col" style="text-align: right;margin-top: 10px;">
                <span class="fullname">Hi, {{ $userData->name }}</span>
                <a href="{{ route('logout') }}" class="logout">Logout</a>
            </div>
        </div>
        <div class="balance">
            <div class="row">
                <div class="col">
                    <div class="balance-left">Transaction Histories</div>
                </div>
                <div class="col">
                    <div class="balance-right">Balance <span>@currency($userData->wallet)<span></div>
                </div>
            </div>
        </div>

        <div class="filter">
            <div class="row">
                <div class="col-10">
                    <div class="filter-left">
                        <form method="GET" action="{{ route('transactionList') }}">
                            <div class="input-group">
                                <input class="form-control border-end-0 border" type="search" name="search"
                                    value="{{ Request::get('search') }}" placeholder="Search" id="example-search-input">
                                <span class="input-group-append">
                                    <button class="btn btn-outline-secondary bg-white border-start-0 border" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-2">
                    <div class="filter-right">
                        <div class="input-group">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary bg-white border ms-n5" type="submit"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal2">
                                    <i class="fas fa-sliders-h"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="add-transaction">
            <div class="row">
                @if (session('errorMessage'))
                    <div class="col-12">
                        <div class="alert alert-danger" style="font-size: 14px;" role="alert">
                            <div class="message-success-register">{!! session('errorMessage') !!}</div>
                        </div>
                    </div>
                @endif
                @if (session('successMessage'))
                    <div class="col-12">
                        <div class="alert alert-success" style="font-size: 14px;" role="alert">
                            <div class="message-success-register">{!! session('successMessage') !!}</div>
                        </div>
                    </div>
                @endif
                <div class="col-12">
                    <div class="add-right"><a href="{{ route('transactionCreate') }}">+Add Transaction</a></div>
                </div>
            </div>
        </div>

        <div class="list-transaction">
            @if (count($transactionData) == 0)
                <div class="list">
                    <div class="row">
                        <div class="col">
                            No transaction found
                        </div>
                    </div>
                </div>
            @endif
            @foreach ($transactionData as $key => $transaction)
                @if ($transaction->type == 'topup')
                    <div class="list">
                        <div class="row">
                            <div class="col-7 title">
                                <i class="fas fa-money-bill-wave" style="color: green"></i>&nbsp;
                                <b>{{ ucfirst($transaction->type) }}</b>
                                <br><span class="transaction-code">#{{ $transaction->code }}</span>
                            </div>
                            <div class="col-5 right upload">
                                <a class="transaction-topup-modal" data-bs-toggle="modal" data-bs-target="#exampleModal"
                                    data="{{ route('transactionImage', ['id' => $transaction->id]) }}"
                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Image Topup Transaction"><i
                                        class="far fa-file-image"></i></a>&nbsp;
                                <a class="transaction-description-modal" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal" data="{{ $transaction->description }}"
                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Description Transaction"><i
                                        class="fas fa-file-alt"></i></a>
                            </div>
                            <div class="col-6">
                                <span
                                    class="transaction-date">{{ date('d M Y H:i:s', strtotime($transaction->created_at)) }}</span>
                            </div>
                            <div class="col-6 right topup">
                                +@currency($transaction->amount)
                            </div>
                        </div>
                    </div>
                @else
                    <div class="list">
                        <div class="row">
                            <div class="col-7 title">
                                <i class="far fa-paper-plane" style="color: orange"></i>&nbsp;
                                <b>{{ ucfirst($transaction->type) }}</b>
                                <br><span class="transaction-code">#{{ $transaction->code }}</span>
                            </div>
                            <div class="col-5 right upload">
                                <a class="transaction-description-modal" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal" data="{{ $transaction->description }}"
                                    data-bs-toggle="tooltip" data-bs-placement="top" title="Description Transaction"><i
                                        class="fas fa-file-alt"></i></a>
                            </div>
                            <div class="col-6">
                                <span
                                    class="transaction-date">{{ date('d M Y H:i:s', strtotime($transaction->created_at)) }}</span>
                            </div>
                            <div class="col-6 right">
                                -@currency($transaction->amount)
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach


            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-end">
                    @for ($i = 1; $i <= $totalPages; $i++)
                        <li
                            class="page-item @if (Request::get('page') == '' && $i == 1) active @endif @if (Request::get('page') == $i) active @endif">
                            <a class="page-link"
                                href="{{ route('transactionList', array_merge(request()->all(), ['page' => $i])) }}">{{ $i }}</a>
                        </li>
                    @endfor
                </ul>
            </nav>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Transaction Detail</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="transaction-modal">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Advance Filter</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="transaction-modal">
                    <form action="{{ route('transactionList', request()->all()) }}" method="GET">
                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="perPage" class="form-label">Per Page</label>
                                <select class="form-select" id="perPage" name="perPage">
                                    <option value="">Choose Per Page</option>
                                    <option value="5" @if (Request::get('perPage') == '5') selected @endif>5</option>
                                    <option value="10" @if (Request::get('perPage') == '10') selected @endif>10</option>
                                    <option value="20" @if (Request::get('perPage') == '20') selected @endif>20</option>
                                    <option value="30" @if (Request::get('perPage') == '30') selected @endif>30</option>
                                    <option value="40" @if (Request::get('perPage') == '40') selected @endif>40</option>
                                    <option value="50" @if (Request::get('perPage') == '50') selected @endif>50</option>
                                    <option value="100" @if (Request::get('perPage') == '100') selected @endif>100</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="sort" class="form-label">Sort</label>
                                <select class="form-select" id="sort" name="sort">
                                    <option value="">Choose Sort</option>
                                    <option value="asc" @if (Request::get('sort') == 'asc') selected @endif>Ascending
                                    </option>
                                    <option value="desc" @if (Request::get('sort') == 'desc') selected @endif>
                                        Descending</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="sortBy" class="form-label">Sort By</label>
                                <select class="form-select" id="sortBy" name="sortBy">
                                    <option value="">Choose Sort By</option>
                                    <option value="type" @if (Request::get('sortBy') == 'type') selected @endif>Type
                                    </option>
                                    <option value="code" @if (Request::get('sortBy') == 'code') selected @endif>
                                        Transaction Code</option>
                                    <option value="created_at" @if (Request::get('sortBy') == 'created_at') selected @endif>
                                        Created At</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="type" class="form-label">Type</label>
                                <select class="form-select" id="type" name="type">
                                    <option value="">Choose Type</option>
                                    <option value="topup" @if (Request::get('type') == 'topup') selected @endif>Topup
                                    </option>
                                    <option value="transaction" @if (Request::get('type') == 'transaction') selected @endif>
                                        Transaction</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="startDate" class="form-label">Start Date</label>
                                <input type="date" class="form-control" id="startDate" name="startDate"
                                    value="{{ Request::get('startDate') }}">
                            </div>
                            <div class="col">
                                <label for="endDate" class="form-label">End Date</label>
                                <input type="date" class="form-control" id="endDate" name="endDate"
                                    value="{{ Request::get('endDate') }}">
                            </div>
                            <div class="col-md-3">
                                <label for="minAmount" class="form-label">Min Amount</label>
                                <input type="text" class="form-control" id="minAmount" name="minAmount"
                                    value="{{ Request::get('minAmount') }}">
                            </div>
                            <div class="col-md-3">
                                <label for="maxAmount" class="form-label">Max Amount</label>
                                <input type="text" class="form-control" id="maxAmount" name="maxAmount"
                                    value="{{ Request::get('maxAmount') }}">
                            </div>
                            <div class="col-md-3">
                                <label for="code" class="form-label">Code</label>
                                <input type="text" class="form-control" id="code" name="code"
                                    value="{{ Request::get('code') }}">
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Search</button>
                                <a href="{{ route('transactionList') }}" type="submit"
                                    class="btn btn-primary">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })()

        $('.transaction-topup-modal').click(function() {
            // Tangkap URL dari atribut data pada tag <a>
            var imageUrl = $(this).attr('data');

            // Setel URL sebagai src untuk tag <img>
            $('#transaction-modal').html('<img src="' + imageUrl +
                '" id="transaction-topup-image" style="max-width: 80%" />').css("text-align", "center");
        });

        $('.transaction-description-modal').click(function() {
            // Tangkap URL dari atribut data pada tag <a>
            var data = $(this).attr('data');

            // Setel URL sebagai src untuk tag <img>
            $('#transaction-modal').html(data).css("text-align", "left");;
        });
    </script>
@endpush
